layui.use('laydate', function(){
    var laydate = layui.laydate;
    //日期时间选择器
    laydate.render({
        elem: '#beginTime'
        ,type: 'datetime'
    });
    laydate.render({
        elem: '#endTime'
        ,type: 'datetime'
    });
});
layui.use('table', function(){
    var table = layui.table;
    // console.log(schoolId)
    table.render({
        elem: '#test'
        ,url:'/manager/studentTable'
        ,where: {
            schoolId: schoolId.val(),
            carId: carId.val()
        }
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            ,groups: 1 //只显示 1 个连续页码
            ,first: false //不显示首页
            ,last: false //不显示尾页

        }
        ,cols: [ [
            {field:'coachName',  title: '学员姓名', sort: true}
            ,{field:'coachSex',  title: '学员性别' , sort: true}
            ,{field:'coachSex',  title: '身份证' , sort: true}
            ,{field:'coachSex',  title: '学员地址' , sort: true}
            ,{field:'createTime',  title: '报名时间' , sort: true}
            ,{field:'coachState',  title: '学生状态', sort: true }
            ,{title: '隶属驾校', sort: true ,templet: '<div><a>{{d.school.schoolName}}</a></div>'}
            ,{title: '教练名称' , sort: true ,templet: '<div><a>{{d.coach.coachName}}</a></div>'}
            ,{fixed: 'right', title:'操作', toolbar: '#barDemo', width:230}
        ] ]
        ,id: 'testReload'
        , page: true
    });
    //点击查询执行重载方法
    var $ = layui.$, active = {
        reload: function () {
            var beginTime = $('#beginTime');
            var endTime = $('#endTime');
            var coachName = $('#coachName');
            var studentName = $('#studentName');
            var schoolName = $('#schoolName');
            //执行重载
            table.reload('testReload', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    beginTime: beginTime.val(),
                    endTime: endTime.val(),
                    coachName: coachName.val(),
                    studentName: studentName.val(),
                    schoolName:schoolName.val(),
                }
            }, 'data');
        },
    };

    //点击事件
    $('.demoTable .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    //监听行工具事件
    table.on('tool(tese)', function(obj){
        if(obj.event === 'viewDetails'){
            console.log(4324312432)
            xadmin.open('教练详情','/page/coachDetails?coachId='+obj.data.coachId);
        }
    });
});

// /**
//  * 返回方法
//  */
// function getBack() {
//     layer.confirm("确定返回吗？",function() {
//         xadmin.close();
//     });
// }
function xadminOpen(carSchoolId) {
    xadmin.open('驾校详情','/page/schoolDetails?schoolId='+carSchoolId,'','',true);
}