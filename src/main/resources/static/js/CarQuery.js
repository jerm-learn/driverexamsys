layui.use('laydate', function(){
    var laydate = layui.laydate;
    //日期时间选择器
    laydate.render({
        elem: '#beginTime'
        ,type: 'datetime'
    });
    laydate.render({
        elem: '#endTime'
        ,type: 'datetime'
    });
});

layui.use('table', function(){
    var table = layui.table;
    table.render({
        elem: '#test'
        ,url:'/manager/carQueryTable'
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            ,groups: 1 //只显示 1 个连续页码
            ,first: false //不显示首页
            ,last: false //不显示尾页

        }
        ,cols: [ [
            {field:'carIdNum',  title: '车牌号', sort: true}
            ,{title: '教练车隶属驾校' , sort: true ,templet: '<div><a>{{d.school.schoolName}}</a></div>'}
            ,{field:'carColor',  title: '教练车颜色' , sort: true}
            ,{field:'carType',  title: '教练车类型' , sort: true}
            ,{field:'carState',  title: '教练车状态', sort: true }
            ,{field:'createTime',  title: '教练车启用时间' , sort: true}
            ,{fixed: 'right', title:'操作', toolbar: '#barDemo', width:230}
        ] ]
        ,id: 'testReload'
        , page: true
    });
    //点击查询执行重载方法
    var $ = layui.$, active = {
        reload: function () {
            var beginTime = $('#beginTime');
            var endTime = $('#endTime');
            var schoolName = $('#schoolName');
            var carIdNum=$('#carIdNum');
            var carType= $('#carType option:selected');
            //执行重载
            table.reload('testReload', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    beginTime: beginTime.val(),
                    endTime: endTime.val(),
                    schoolName: schoolName.val(),
                    carIdNum:carIdNum.val(),
                    carType:carType.val(),
                }
            }, 'data');
        },
    };

    //点击事件
    $('.demoTable .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    //事件重载方法
    function load(curPage){
        var beginTime = $('#beginTime');
        var endTime = $('#endTime');
        var schoolName = $('#schoolName');
        //执行重载
        table.reload('testReload', {
            page: {
                curr: curPage
            }
            , where: {
                beginTime: beginTime.val(),
                endTime: endTime.val(),
                schoolName: schoolName.val()
            }
        }, 'data');
    }




    //监听行工具事件
    table.on('tool(tese)', function(obj){
        var curPage = $(".layui-laypage-em").next().html();
        console.log(curPage)
        if(obj.event === 'viewDetails'){
            xadmin.open('教练车详情','/page/carDetails?carId='+obj.data.carId);
        }
    });
});