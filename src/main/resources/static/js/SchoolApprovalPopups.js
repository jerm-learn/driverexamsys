/**
 * 审核方法
 * @param state
 */
function approval(state) {
    var schoolId = $("#schoolId").val();
    console.log(state+"===="+schoolId)
    layer.confirm("确定"+state+"吗",function() {
        $.ajax({
            url:"/manager/approval",
            type:"post",
            data:"schoolId="+schoolId+"&schoolState="+state,
            dataType:"text",
            success:function (data) {
                layer.open({
                    type: 1
                    ,offset: 'auto' //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
                    ,id: 'layerDemo' //防止重复弹出
                    ,content: '<div style="padding: 20px 100px;">'+data +'</div>'
                    ,btn: '确定'
                    ,btnAlign: 'c' //按钮居中
                    ,shade: 0 //不显示遮罩
                    ,yes: function(){
                        if(data=="审核成功"){
                            xadmin.close();
                            parent.document.getElementById("select").click();
                        }
                    }
                });
            },
            error:function () {
                alert('网络繁忙');
            },
        });
    })

}

