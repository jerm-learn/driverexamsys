package com.cykj.bean;

public class Log {
    private int logId;
    private int logUserId;
    private String logModule;
    private String logAction;
    private String logTime;

    public Log() {
    }

    public Log(int logId, int logUserId, String logModule, String logAction, String logTime) {
        this.logId = logId;
        this.logUserId = logUserId;
        this.logModule = logModule;
        this.logAction = logAction;
        this.logTime = logTime;
    }

    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    public int getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(int logUserId) {
        this.logUserId = logUserId;
    }

    public String getLogModule() {
        return logModule;
    }

    public void setLogModule(String logModule) {
        this.logModule = logModule;
    }

    public String getLogAction() {
        return logAction;
    }

    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }
}
