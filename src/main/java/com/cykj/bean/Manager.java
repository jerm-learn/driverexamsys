package com.cykj.bean;
//运营管理员
public class Manager {
    private int managerId;
    private int userId;
    private String managerName;
    private String managerPhone;
    private String managerImg;
    private String managerTime;

    public Manager() {
    }

    public Manager(int managerId, int userId, String managerName, String managerPhone, String managerImg, String managerTime) {
        this.managerId = managerId;
        this.userId = userId;
        this.managerName = managerName;
        this.managerPhone = managerPhone;
        this.managerImg = managerImg;
        this.managerTime = managerTime;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public String getManagerImg() {
        return managerImg;
    }

    public void setManagerImg(String managerImg) {
        this.managerImg = managerImg;
    }

    public String getManagerTime() {
        return managerTime;
    }

    public void setManagerTime(String managerTime) {
        this.managerTime = managerTime;
    }
}
