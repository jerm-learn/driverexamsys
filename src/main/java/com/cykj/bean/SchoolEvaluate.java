package com.cykj.bean;
//学院评价
public class SchoolEvaluate {
    private int schoolEvaluateId;
    private int schoolId;
    private String schoolEvaluateContent;//内容
    private String schoolEvaluateDate;
    private int schoolEvaluateScore;

    public SchoolEvaluate() {
    }

    public SchoolEvaluate(int schoolEvaluateId, int schoolId, String schoolEvaluateContent, String schoolEvaluateDate, int schoolEvaluateScore) {
        this.schoolEvaluateId = schoolEvaluateId;
        this.schoolId = schoolId;
        this.schoolEvaluateContent = schoolEvaluateContent;
        this.schoolEvaluateDate = schoolEvaluateDate;
        this.schoolEvaluateScore = schoolEvaluateScore;
    }

    public int getSchoolEvaluateId() {
        return schoolEvaluateId;
    }

    public void setSchoolEvaluateId(int schoolEvaluateId) {
        this.schoolEvaluateId = schoolEvaluateId;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolEvaluateContent() {
        return schoolEvaluateContent;
    }

    public void setSchoolEvaluateContent(String schoolEvaluateContent) {
        this.schoolEvaluateContent = schoolEvaluateContent;
    }

    public String getSchoolEvaluateDate() {
        return schoolEvaluateDate;
    }

    public void setSchoolEvaluateDate(String schoolEvaluateDate) {
        this.schoolEvaluateDate = schoolEvaluateDate;
    }

    public int getSchoolEvaluateScore() {
        return schoolEvaluateScore;
    }

    public void setSchoolEvaluateScore(int schoolEvaluateScore) {
        this.schoolEvaluateScore = schoolEvaluateScore;
    }
}
