package com.cykj.bean;
//考试安排
public class ExamArr {
    private int examArrId;
    private int levelId;
    private int studentId;
    private String studentDate;
    private String studentPlace;
    private String studentState;

    public ExamArr() {
    }

    public ExamArr(int examArrId, int levelId, int studentId, String studentDate, String studentPlace, String studentState) {
        this.examArrId = examArrId;
        this.levelId = levelId;
        this.studentId = studentId;
        this.studentDate = studentDate;
        this.studentPlace = studentPlace;
        this.studentState = studentState;
    }

    public int getExamArrId() {
        return examArrId;
    }

    public void setExamArrId(int examArrId) {
        this.examArrId = examArrId;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentDate() {
        return studentDate;
    }

    public void setStudentDate(String studentDate) {
        this.studentDate = studentDate;
    }

    public String getStudentPlace() {
        return studentPlace;
    }

    public void setStudentPlace(String studentPlace) {
        this.studentPlace = studentPlace;
    }

    public String getStudentState() {
        return studentState;
    }

    public void setStudentState(String studentState) {
        this.studentState = studentState;
    }
}
