package com.cykj.bean;

public class Student {
    private int studentId;
    private int userId;
    private int coachId;//教练id
    private int subjectTimeId;//学时id
    private String studentName;
    private String studentSex;
    private String studentIdNum;//身份证
    private String studentBirth;//生日
    private String studentAddress;
    private String studentState;
    private String createTime;
    private School school;
    private Coach coach;

    public Student() {
    }

    public Student(int studentId, int userId, int coachId, int subjectTimeId, String studentName, String studentSex, String studentIdNum, String studentBirth, String studentAddress, String studentState) {
        this.studentId = studentId;
        this.userId = userId;
        this.coachId = coachId;
        this.subjectTimeId = subjectTimeId;
        this.studentName = studentName;
        this.studentSex = studentSex;
        this.studentIdNum = studentIdNum;
        this.studentBirth = studentBirth;
        this.studentAddress = studentAddress;
        this.studentState = studentState;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public int getSubjectTimeId() {
        return subjectTimeId;
    }

    public void setSubjectTimeId(int subjectTimeId) {
        this.subjectTimeId = subjectTimeId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        this.studentSex = studentSex;
    }

    public String getStudentIdNum() {
        return studentIdNum;
    }

    public void setStudentIdNum(String studentIdNum) {
        this.studentIdNum = studentIdNum;
    }

    public String getStudentBirth() {
        return studentBirth;
    }

    public void setStudentBirth(String studentBirth) {
        this.studentBirth = studentBirth;
    }

    public String getStudentAddress() {
        return studentAddress;
    }

    public void setStudentAddress(String studentAddress) {
        this.studentAddress = studentAddress;
    }

    public String getStudentState() {
        return studentState;
    }

    public void setStudentState(String studentState) {
        this.studentState = studentState;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
