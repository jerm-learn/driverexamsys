package com.cykj.bean;

public class Topic {
    private int topicId;
    private String topicTitle;
    private String topicOptionA;
    private String topicOptionB;
    private String topicOptionC;
    private String topicOptionD;
    private String topicAnswer;
    private String topicImg;

    public Topic() {
    }

    public Topic(int topicId, String topicTitle, String topicOptionA, String topicOptionB, String topicOptionC, String topicOptionD, String topicAnswer, String topicImg) {
        this.topicId = topicId;
        this.topicTitle = topicTitle;
        this.topicOptionA = topicOptionA;
        this.topicOptionB = topicOptionB;
        this.topicOptionC = topicOptionC;
        this.topicOptionD = topicOptionD;
        this.topicAnswer = topicAnswer;
        this.topicImg = topicImg;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public String getTopicOptionA() {
        return topicOptionA;
    }

    public void setTopicOptionA(String topicOptionA) {
        this.topicOptionA = topicOptionA;
    }

    public String getTopicOptionB() {
        return topicOptionB;
    }

    public void setTopicOptionB(String topicOptionB) {
        this.topicOptionB = topicOptionB;
    }

    public String getTopicOptionC() {
        return topicOptionC;
    }

    public void setTopicOptionC(String topicOptionC) {
        this.topicOptionC = topicOptionC;
    }

    public String getTopicOptionD() {
        return topicOptionD;
    }

    public void setTopicOptionD(String topicOptionD) {
        this.topicOptionD = topicOptionD;
    }

    public String getTopicAnswer() {
        return topicAnswer;
    }

    public void setTopicAnswer(String topicAnswer) {
        this.topicAnswer = topicAnswer;
    }

    public String getTopicImg() {
        return topicImg;
    }

    public void setTopicImg(String topicImg) {
        this.topicImg = topicImg;
    }
}
