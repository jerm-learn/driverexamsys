package com.cykj.bean;

public class School {
    private int schoolId;
    private int userId;
    private String schoolName;
    private String schoolPhone;
    private String schoolDate;
    private String schoolMaster;
    private String schoolCode;
    private String schoolImg;
    private String schoolIntroduce;//介绍
    private String schoolAddress;
    private String schoolState;
    private String schoolApprovalTime;

    public School() {
    }

    public School(int schoolId, int userId, String schoolName, String schoolPhone, String schoolDate, String schoolMaster, String schoolCode, String schoolImg, String schoolIntroduce, String schoolAddress, String schoolState, String schoolApprovalTime) {
        this.schoolId = schoolId;
        this.userId = userId;
        this.schoolName = schoolName;
        this.schoolPhone = schoolPhone;
        this.schoolDate = schoolDate;
        this.schoolMaster = schoolMaster;
        this.schoolCode = schoolCode;
        this.schoolImg = schoolImg;
        this.schoolIntroduce = schoolIntroduce;
        this.schoolAddress = schoolAddress;
        this.schoolState = schoolState;
        this.schoolApprovalTime = schoolApprovalTime;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolPhone() {
        return schoolPhone;
    }

    public void setSchoolPhone(String schoolPhone) {
        this.schoolPhone = schoolPhone;
    }

    public String getSchoolDate() {
        return schoolDate;
    }

    public void setSchoolDate(String schoolDate) {
        this.schoolDate = schoolDate;
    }

    public String getSchoolMaster() {
        return schoolMaster;
    }

    public void setSchoolMaster(String schoolMaster) {
        this.schoolMaster = schoolMaster;
    }

    public String getSchoolCode() {
        return schoolCode;
    }

    public void setSchoolCode(String schoolCode) {
        this.schoolCode = schoolCode;
    }

    public String getSchoolImg() {
        return schoolImg;
    }

    public void setSchoolImg(String schoolImg) {
        this.schoolImg = schoolImg;
    }

    public String getSchoolIntroduce() {
        return schoolIntroduce;
    }

    public void setSchoolIntroduce(String schoolIntroduce) {
        this.schoolIntroduce = schoolIntroduce;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public String getSchoolState() {
        return schoolState;
    }

    public void setSchoolState(String schoolState) {
        this.schoolState = schoolState;
    }

    public String getSchoolApprovalTime() {
        return schoolApprovalTime;
    }

    public void setSchoolApprovalTime(String schoolApprovalTime) {
        this.schoolApprovalTime = schoolApprovalTime;
    }
}
