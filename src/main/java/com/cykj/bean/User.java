package com.cykj.bean;

public class User {
    private int userId;
    private int roleId;
    private String userAccount;
    private String userPwd;
    private String userState;

    public User() {
    }

    public User(int userId, int roleId, String userAccount, String userPwd, String userState) {
        this.userId = userId;
        this.roleId = roleId;
        this.userAccount = userAccount;
        this.userPwd = userPwd;
        this.userState = userState;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }
}
