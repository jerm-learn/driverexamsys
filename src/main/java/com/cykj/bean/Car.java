package com.cykj.bean;

public class Car {
    private int carId;
    private int schoolId;
    private int userId;
    private String carIdNum;//车牌号
    private String carColor;
    private String carState;
    private String carType;//A1 A2 类车型
    private String createTime;
    private School school;

    public Car() {
    }

    public Car(int carId, int schoolId, int userId, String carIdNum, String carColor, String carState, String carType) {
        this.carId = carId;
        this.schoolId = schoolId;
        this.userId = userId;
        this.carIdNum = carIdNum;
        this.carColor = carColor;
        this.carState = carState;
        this.carType = carType;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCarIdNum() {
        return carIdNum;
    }

    public void setCarIdNum(String carIdNum) {
        this.carIdNum = carIdNum;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarState() {
        return carState;
    }

    public void setCarState(String carState) {
        this.carState = carState;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
