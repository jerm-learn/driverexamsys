package com.cykj.bean;

public class CoachEvaluate {
    private int coachEvaluateId;
    private int studentId;
    private int levelId;
    private String coachEvaluateContent;
    private String coachEvaluateDate;
    private int coachEvaluateScore;

    public CoachEvaluate() {
    }

    public CoachEvaluate(int coachEvaluateId, int studentId, int levelId, String coachEvaluateContent, String coachEvaluateDate, int coachEvaluateScore) {
        this.coachEvaluateId = coachEvaluateId;
        this.studentId = studentId;
        this.levelId = levelId;
        this.coachEvaluateContent = coachEvaluateContent;
        this.coachEvaluateDate = coachEvaluateDate;
        this.coachEvaluateScore = coachEvaluateScore;
    }

    public int getCoachEvaluateId() {
        return coachEvaluateId;
    }

    public void setCoachEvaluateId(int coachEvaluateId) {
        this.coachEvaluateId = coachEvaluateId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public String getCoachEvaluateContent() {
        return coachEvaluateContent;
    }

    public void setCoachEvaluateContent(String coachEvaluateContent) {
        this.coachEvaluateContent = coachEvaluateContent;
    }

    public String getCoachEvaluateDate() {
        return coachEvaluateDate;
    }

    public void setCoachEvaluateDate(String coachEvaluateDate) {
        this.coachEvaluateDate = coachEvaluateDate;
    }

    public int getCoachEvaluateScore() {
        return coachEvaluateScore;
    }

    public void setCoachEvaluateScore(int coachEvaluateScore) {
        this.coachEvaluateScore = coachEvaluateScore;
    }
}
