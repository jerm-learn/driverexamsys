package com.cykj.bean;

public class Coach {
    private int coachId;
    private int userId;
    private int schoolId;
    private int carId;
    private String coachName;
    private String coachSex;
    private String coachPhone;
    private String coachState;
    private String coachIdNum;
    private String coachImg;
    private String coachIntroduce;//简介
    private Car car;
    private School school;
    private String createTime;

    public Coach() {
    }

    public Coach(int coachId, int userId, int schoolId, int carId, String coachName, String coachSex, String coachPhone, String coachState, String coachIdNum, String coachImg, String coachIntroduce) {
        this.coachId = coachId;
        this.userId = userId;
        this.schoolId = schoolId;
        this.carId = carId;
        this.coachName = coachName;
        this.coachSex = coachSex;
        this.coachPhone = coachPhone;
        this.coachState = coachState;
        this.coachIdNum = coachIdNum;
        this.coachImg = coachImg;
        this.coachIntroduce = coachIntroduce;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getCoachSex() {
        return coachSex;
    }

    public void setCoachSex(String coachSex) {
        this.coachSex = coachSex;
    }

    public String getCoachPhone() {
        return coachPhone;
    }

    public void setCoachPhone(String coachPhone) {
        this.coachPhone = coachPhone;
    }

    public String getCoachState() {
        return coachState;
    }

    public void setCoachState(String coachState) {
        this.coachState = coachState;
    }

    public String getCoachIdNum() {
        return coachIdNum;
    }

    public void setCoachIdNum(String coachIdNum) {
        this.coachIdNum = coachIdNum;
    }

    public String getCoachImg() {
        return coachImg;
    }

    public void setCoachImg(String coachImg) {
        this.coachImg = coachImg;
    }

    public String getCoachIntroduce() {
        return coachIntroduce;
    }

    public void setCoachIntroduce(String coachIntroduce) {
        this.coachIntroduce = coachIntroduce;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
