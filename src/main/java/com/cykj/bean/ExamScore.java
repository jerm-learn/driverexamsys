package com.cykj.bean;

public class ExamScore {
    private int examId;
    private int examArrId;
    private String examScore1;
    private String examScore2;

    public ExamScore() {
    }

    public ExamScore(int examId, int examArrId, String examScore1, String examScore2) {
        this.examId = examId;
        this.examArrId = examArrId;
        this.examScore1 = examScore1;
        this.examScore2 = examScore2;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public int getExamArrId() {
        return examArrId;
    }

    public void setExamArrId(int examArrId) {
        this.examArrId = examArrId;
    }

    public String getExamScore1() {
        return examScore1;
    }

    public void setExamScore1(String examScore1) {
        this.examScore1 = examScore1;
    }

    public String getExamScore2() {
        return examScore2;
    }

    public void setExamScore2(String examScore2) {
        this.examScore2 = examScore2;
    }
}
