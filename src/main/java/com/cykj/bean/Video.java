package com.cykj.bean;

public class Video {
    private int videoId;
    private String videoName;
    private String videoUrl;

    public Video() {
    }

    public Video(int videoId, String videoName, String videoUrl) {
        this.videoId = videoId;
        this.videoName = videoName;
        this.videoUrl = videoUrl;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
