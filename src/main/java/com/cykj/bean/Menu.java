package com.cykj.bean;

public class Menu {
    private int menuId;
    private int roleId;
    private int menuPid;
    private String menuName;
    private String menuUrl;

    public Menu() {
    }

    public Menu(int menuId, int roleId, int menuPid, String menuName, String menuUrl) {
        this.menuId = menuId;
        this.roleId = roleId;
        this.menuPid = menuPid;
        this.menuName = menuName;
        this.menuUrl = menuUrl;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getMenuPid() {
        return menuPid;
    }

    public void setMenuPid(int menuPid) {
        this.menuPid = menuPid;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }
}
