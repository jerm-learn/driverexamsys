package com.cykj.bean;

public class Message {
    private int messageId;
    private String messageContent;
    private String messageTime;

    public Message() {
    }

    public Message(int messageId, String messageContent, String messageTime) {
        this.messageId = messageId;
        this.messageContent = messageContent;
        this.messageTime = messageTime;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }
}
