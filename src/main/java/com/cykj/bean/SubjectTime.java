package com.cykj.bean;

public class SubjectTime {
    private int SubjectTimeId;
    private int videoId;
    private int levelId;
    private String SubjectTimeDate;
    private int SubjectTimeSum;
    private int SubjectTimeValid;

    public SubjectTime() {
    }

    public SubjectTime(int subjectTimeId, int videoId, int levelId, String subjectTimeDate, int subjectTimeSum, int subjectTimeValid) {
        SubjectTimeId = subjectTimeId;
        this.videoId = videoId;
        this.levelId = levelId;
        SubjectTimeDate = subjectTimeDate;
        SubjectTimeSum = subjectTimeSum;
        SubjectTimeValid = subjectTimeValid;
    }

    public int getSubjectTimeId() {
        return SubjectTimeId;
    }

    public void setSubjectTimeId(int subjectTimeId) {
        SubjectTimeId = subjectTimeId;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public String getSubjectTimeDate() {
        return SubjectTimeDate;
    }

    public void setSubjectTimeDate(String subjectTimeDate) {
        SubjectTimeDate = subjectTimeDate;
    }

    public int getSubjectTimeSum() {
        return SubjectTimeSum;
    }

    public void setSubjectTimeSum(int subjectTimeSum) {
        SubjectTimeSum = subjectTimeSum;
    }

    public int getSubjectTimeValid() {
        return SubjectTimeValid;
    }

    public void setSubjectTimeValid(int subjectTimeValid) {
        SubjectTimeValid = subjectTimeValid;
    }
}
