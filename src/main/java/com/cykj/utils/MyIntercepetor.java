package com.cykj.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class MyIntercepetor implements HandlerInterceptor {

    //true  则放行  执行下一个拦截器
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        //放行的情况：判断什么情况下登入了
        HttpSession session = request.getSession();
        System.out.println("session不等于null：");
        System.out.println(session.getAttribute("user")!=null);
        //1、判断session有值就放行
        if(session.getAttribute("user")!=null){
            return true;
        }
        //2、登入页面也会放行
        System.out.println("请求是否包含login：");
        System.out.println(request.getRequestURI().contains("login"));
        if(request.getRequestURI().contains("login")){
            return true;
        }
        request.setAttribute("message","权限不足，请先登入");
        //如果没有登入  让他返回登入页面
        request.getRequestDispatcher("/page/login").forward(request,response);
        return false;
    }
}
