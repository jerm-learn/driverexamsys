package com.cykj.mapper;

import com.cykj.bean.Car;
import com.cykj.bean.Coach;
import com.cykj.bean.School;
import com.cykj.bean.Student;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ManagerMapper {
    //SchoolPenalize页面表格数据数量
    List<School> findSchoolPenalizeTable(@Param("params") Map<String, Object> map, RowBounds rb);
    int schoolPenalizeTablePage(@Param("params") Map<String, Object> map);
    //修改学院状态
    int alterSchoolState(@Param("schoolId") int schoolId,@Param("state") String state);
    //SchoolApproval页面表格数据数量
    List<School> findSchoolApprovalTable(@Param("params") Map<String, Object> map, RowBounds rb);
    int schoolApprovalTablePage(@Param("params") Map<String, Object> map);
    //查找学校
    School findSchool(@Param("schoolId")int id);
    //审批学院状态
    int approvalSchoolState(@Param("schoolId") int schoolId,@Param("state") String state,@Param("datetime")String datetime);
    //查找学校教练表格
    List<Coach> schoolCoachTable(@Param("params") Map<String, Object> map, RowBounds rb);
    int schoolCoachTablePage(@Param("params") Map<String, Object> map);
    //查找教练详情
    Coach findCoach(@Param("coachId")int id);
    //carQuery页面表格数据数量
    List<Car> carQueryTable(@Param("params") Map<String, Object> map, RowBounds rb);
    int carQueryTablePage(@Param("params") Map<String, Object> map);
    //查找教练车详情
    Car findCar(@Param("carId")int id);
    //student页面表格数据数量
    List<Student> studentTable(@Param("params") Map<String, Object> map, RowBounds rb);
    int studentTablePage(@Param("params") Map<String, Object> map);

}
