package com.cykj.service.Impl;

import com.cykj.bean.Car;
import com.cykj.bean.Coach;
import com.cykj.bean.School;
import com.cykj.bean.Student;
import com.cykj.mapper.ManagerMapper;
import com.cykj.service.ManagerService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
@Transactional
public class ManagerServiceImpl implements ManagerService {
    @Autowired
    private ManagerMapper managerMapper;

    /**
     * 查询SchoolPenalizeTable的数据
     * @param condition
     * @param rb
     * @return
     */
    @Override
    public List<School> findSchoolPenalizeTable(HashMap<String, Object> condition, RowBounds rb) {
        List<School> schoolList =managerMapper.findSchoolPenalizeTable(condition,rb);
        return schoolList;
    }

    /**
     * 查询SchoolPenalizeTable的页数
     * @param condition
     * @return
     */
    @Override
    public int schoolPenalizeTablePage(HashMap<String, Object> condition) {
        int n =managerMapper.schoolPenalizeTablePage(condition);
        return n;
    }

    /**
     * 修改驾校状态
     * @param schoolId
     * @param state
     * @return
     */
    @Override
    public int alterSchoolState(int schoolId, String state) {
        int n =managerMapper.alterSchoolState(schoolId,state);
        return n;
    }

    /**
     * 查询SchoolApprovalTable的数据
     * @param condition
     * @param rb
     * @return
     */
    @Override
    public List<School> findSchoolApprovalTable(HashMap<String, Object> condition, RowBounds rb) {
        List<School> schoolList =managerMapper.findSchoolApprovalTable(condition,rb);
        return schoolList;
    }

    /**
     * 查询SchoolApprovalTable的页数
     * @param condition
     * @return
     */
    @Override
    public int schoolApprovalTablePage(HashMap<String, Object> condition) {
        int n =managerMapper.schoolApprovalTablePage(condition);
        return n;
    }

    /**
     * 查找学校
     * @param id
     * @return
     */
    @Override
    public School findSchool(int id) {
        School school =managerMapper.findSchool(id);
        return school;
    }

    /**
     * 审批驾校状态
     * @param schoolId
     * @param state
     * @return
     */
    @Override
    public int approvalSchoolState(int schoolId, String state) {
        System.out.println(state);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String datetime = sdf.format(new Date());
        int n =managerMapper.approvalSchoolState(schoolId,state,datetime);
        return n;
    }

    //通过找教练table数据
    @Override
    public List<Coach> schoolCoachTable(HashMap<String,Object> condition, RowBounds rb) {
        List<Coach> coachList =managerMapper.schoolCoachTable(condition,rb);
        return coachList;
    }
    @Override
    public int schoolCoachTablePage(HashMap<String,Object> condition) {
        int n =managerMapper.schoolCoachTablePage(condition);
        return n;
    }


    /**
     * 找到教练详情信息
     * @param id
     * @return
     */
    @Override
    public Coach findCoach(int id) {
        Coach coach =managerMapper.findCoach(id);
        return coach;
    }
    // 教练车table信息
    @Override
    public List<Car> carQueryTable(HashMap<String, Object> condition, RowBounds rb) {
        List<Car> carsList =managerMapper.carQueryTable(condition,rb);
        return carsList;
    }
    @Override
    public int carQueryTablePage(HashMap<String, Object> condition) {
        int n =managerMapper.carQueryTablePage(condition);
        return n;
    }

    /**
     * 找到教练车详情信息
     * @param id
     * @return
     */
    @Override
    public Car findCar(int id) {
        Car car =managerMapper.findCar(id);
        return car;
    }

    // 学生table信息
    @Override
    public List<Student> studentTable(HashMap<String, Object> condition, RowBounds rb) {
        List<Student> studentList =managerMapper.studentTable(condition,rb);
        return studentList;
    }
    @Override
    public int studentTablePage(HashMap<String, Object> condition) {
        int n =managerMapper.studentTablePage(condition);
        return n;
    }
}
