package com.cykj.service;

import com.cykj.bean.Car;
import com.cykj.bean.Coach;
import com.cykj.bean.School;
import com.cykj.bean.Student;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;

public interface ManagerService {
    //SchoolPenalize页面表格数据数量
    List<School> findSchoolPenalizeTable(HashMap<String,Object> condition, RowBounds rb);
    int schoolPenalizeTablePage(HashMap<String,Object> condition);
    //修改学院状态
    int alterSchoolState(int schoolId,String state);
    //SchoolApproval页面表格数据数量
    List<School> findSchoolApprovalTable(HashMap<String,Object> condition, RowBounds rb);
    int schoolApprovalTablePage(HashMap<String,Object> condition);
    //查找学校
    School findSchool(int id);
    //审批学院状态
    int approvalSchoolState(int schoolId,String state);
    //教练信息查询
    List<Coach> schoolCoachTable(HashMap<String,Object> condition,  RowBounds rb);
    int schoolCoachTablePage(HashMap<String,Object> condition);

    //查找教练
    Coach findCoach(int id);
    //carQueryTable
    List<Car> carQueryTable(HashMap<String,Object> condition, RowBounds rb);
    int carQueryTablePage(HashMap<String,Object> condition);
    //查找教练车
    Car findCar(int id);
    //carQueryTable
    List<Student> studentTable(HashMap<String,Object> condition, RowBounds rb);
    int studentTablePage(HashMap<String,Object> condition);

}
