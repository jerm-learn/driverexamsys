package com.cykj;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cykj.mapper")
public class DriverexamsysApplication {

    public static void main(String[] args) {
        SpringApplication.run(DriverexamsysApplication.class, args);
    }

}
