package com.cykj.controller;

import com.cykj.bean.*;
import com.cykj.service.Impl.ManagerServiceImpl;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/manager",  produces = { "application/json;charset=UTF-8" })
public class ManagerController {
    @Autowired
    private ManagerServiceImpl managerService;

    /**
     * SchoolPenalize页面表格
     * @param request
     * @param limit
     * @param page
     * @param beginTime
     * @param endTime
     * @param schoolName
     * @return
     */
    @RequestMapping("/schoolPenalizeTable")
    @ResponseBody
    public LayuiJson<School> schoolPenalizeTable(HttpServletRequest request,String limit,String page,String beginTime,String endTime,String schoolName){
        System.out.println("收到驾校table数据请求");
        //添加查询数据到map集合
        HashMap<String,Object> condition =new HashMap<>();
        if(schoolName!=null && !schoolName.trim().equals("")){
            condition.put("schoolName",schoolName);
        }
        if(beginTime!=null && !beginTime.trim().equals("")){
            condition.put("beginTime",beginTime);
        }
        if(endTime!=null && !endTime.trim().equals("")){
            condition.put("endTime",endTime);
        }
        RowBounds rb =new RowBounds((Integer.parseInt(page)-1)*Integer.parseInt(limit),Integer.parseInt(limit));
        List<School> schoolList =managerService.findSchoolPenalizeTable(condition,rb);
        int n =managerService.schoolPenalizeTablePage(condition);
        return new LayuiJson<School>(n,schoolList);
    }

    @RequestMapping("/schoolState")
    @ResponseBody
    public String alterSchoolState(HttpServletRequest request,String schoolId,String state){
        System.out.println("收到驾校状态修改请求");
        int id =Integer.parseInt(schoolId);
        int n =managerService.alterSchoolState(id,state);
        if(n>0){
//            MyLogUtil.addLog("管理员模块","审批文档",docId+"文档"+docState,user.getId());
            return "修改成功";
        }else {
            return "修改失败";
        }
    }

    /**
     * schoolApproval页面表格
     * @param request
     * @param limit
     * @param page
     * @param beginTime
     * @param endTime
     * @param schoolName
     * @return
     */
    @RequestMapping("/schoolApproval")
    @ResponseBody
    public LayuiJson<School> schoolApprovalTable(HttpServletRequest request,String limit,String page,String beginTime,String endTime,String schoolName){
        System.out.println("收到驾校审批TABLE获取请求");
        //添加查询数据到map集合
        HashMap<String,Object> condition =new HashMap<>();
        if(schoolName!=null && !schoolName.trim().equals("")){
            condition.put("schoolName",schoolName);
        }
        if(beginTime!=null && !beginTime.trim().equals("")){
            condition.put("beginTime",beginTime);
        }
        if(endTime!=null && !endTime.trim().equals("")){
            condition.put("endTime",endTime);
        }
        RowBounds rb =new RowBounds((Integer.parseInt(page)-1)*Integer.parseInt(limit),Integer.parseInt(limit));
        List<School> schoolList =managerService.findSchoolApprovalTable(condition,rb);
        int n =managerService.schoolApprovalTablePage(condition);
        return new LayuiJson<School>(n,schoolList);
    }

    /**
     * 文件审核
     * @param request
     * @param schoolId
     * @param schoolState
     * @return
     */
    @RequestMapping("/approval")
    @ResponseBody
    public String approvalSchoolState(HttpServletRequest request,String schoolId,String schoolState){
        int id =Integer.parseInt(schoolId);
        int n =managerService.approvalSchoolState(id,schoolState);
        if(n>0){
//            MyLogUtil.addLog("管理员模块","审批文档",docId+"文档"+docState,user.getId());
            return "审核成功";
        }else {
            return "审核失败";
        }
    }

    /**
     * 驾校教练table
     * @param request
     * @param limit
     * @param page
     * @param schoolId
     * @return
     */
    @RequestMapping("/schoolCoachTable")
    @ResponseBody
    public LayuiJson<Coach> schoolCoachTable(HttpServletRequest request, String limit, String page,String schoolId,String carId,String beginTime,String endTime,String coachName,String carIdNum){
        System.out.println("收到教练信息请求"+schoolId+"="+carId);
        HashMap<String,Object> condition =new HashMap<>();
        if(schoolId!=null && !schoolId.trim().equals("")){
            condition.put("schoolId",schoolId);
        }
        if(carId!=null && !carId.trim().equals("")){
            condition.put("carId",carId);
        }
        if(beginTime!=null && !beginTime.trim().equals("")){
            condition.put("beginTime",beginTime);
        }
        if(endTime!=null && !endTime.trim().equals("")){
            condition.put("endTime",endTime);
        }
        if(coachName!=null && !coachName.trim().equals("")){
            condition.put("coachName",coachName);
        }
        if(carIdNum!=null && !carIdNum.trim().equals("")){
            condition.put("carIdNum",carIdNum);
        }
        System.out.println(condition.size());
        //添加查询数据到map集合
        RowBounds rb =new RowBounds((Integer.parseInt(page)-1)*Integer.parseInt(limit),Integer.parseInt(limit));
        List<Coach> coachList =managerService.schoolCoachTable(condition,rb);
        int n =managerService.schoolCoachTablePage(condition);
        return new LayuiJson<Coach>(n,coachList);
    }


    @RequestMapping("/carQueryTable")
    @ResponseBody
    public LayuiJson<Car> carQueryTable(HttpServletRequest request,String limit,String page,String beginTime,String endTime,String schoolName,String carIdNum,String carType){
        System.out.println("收到教练车信息请求");
        //添加查询数据到map集合
        HashMap<String,Object> condition =new HashMap<>();
        if(schoolName!=null && !schoolName.trim().equals("")){
            condition.put("schoolName",schoolName);
        }
        if(beginTime!=null && !beginTime.trim().equals("")){
            condition.put("beginTime",beginTime);
        }
        if(endTime!=null && !endTime.trim().equals("")){
            condition.put("endTime",endTime);
        }
        if(carIdNum!=null && !carIdNum.trim().equals("")){
            condition.put("carIdNum",carIdNum);
        }
        if(carType!=null && !carType.trim().equals("")){
            condition.put("carType",carType);
        }
        RowBounds rb =new RowBounds((Integer.parseInt(page)-1)*Integer.parseInt(limit),Integer.parseInt(limit));
        List<Car> carList =managerService.carQueryTable(condition,rb);
        int n =managerService.carQueryTablePage(condition);
        return new LayuiJson<Car>(n,carList);
    }

    @RequestMapping("/studentTable")
    @ResponseBody
    public LayuiJson<Student> studentTable(HttpServletRequest request, String limit, String page, String beginTime, String endTime, String schoolName, String coachName, String studentName){
        System.out.println("收到学员信息请求");
        //添加查询数据到map集合
        HashMap<String,Object> condition =new HashMap<>();
        if(schoolName!=null && !schoolName.trim().equals("")){
            condition.put("schoolName",schoolName);
        }
        if(beginTime!=null && !beginTime.trim().equals("")){
            condition.put("beginTime",beginTime);
        }
        if(endTime!=null && !endTime.trim().equals("")){
            condition.put("endTime",endTime);
        }
        if(coachName!=null && !coachName.trim().equals("")){
            condition.put("coachName",coachName);
        }
        if(studentName!=null && !studentName.trim().equals("")){
            condition.put("studentName",studentName);
        }
        RowBounds rb =new RowBounds((Integer.parseInt(page)-1)*Integer.parseInt(limit),Integer.parseInt(limit));
        List<Student> studentList =managerService.studentTable(condition,rb);
        int n =managerService.studentTablePage(condition);
        return new LayuiJson<Student>();
    }

}
