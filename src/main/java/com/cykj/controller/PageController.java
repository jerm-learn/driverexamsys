package com.cykj.controller;

import com.cykj.bean.Car;
import com.cykj.bean.Coach;
import com.cykj.bean.School;
import com.cykj.service.Impl.ManagerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/page",  produces = { "application/json;charset=UTF-8" })
public class PageController {

    @Autowired
    private ManagerServiceImpl managerService;

    @RequestMapping("/managerBackground")
    public String schoolPenalizePage(){
        return "ManagerBackground";
    }

    @RequestMapping("/redirect")
    public String findPage(@RequestParam String pageName){
        System.out.println("跳转页面"+pageName);
        return pageName;
    }

    @RequestMapping("/schoolApprovalPopups")
    public String SchoolApprovalPopups(Model model, String schoolId){
        System.out.println("学校数据请求");
        int id= Integer.parseInt(schoolId);
        School school =managerService.findSchool(id);
        model.addAttribute("school",school);
        return "SchoolApprovalPopups";
    }
    @RequestMapping("/schoolDetails")
    public String schoolDetails(Model model, String schoolId){
        System.out.println("学校数据请求");
        int id= Integer.parseInt(schoolId);
        School school =managerService.findSchool(id);
        model.addAttribute("school",school);
        return "SchoolDetails";
    }
    @RequestMapping("/coachDetails")
    public String coachDetails(Model model, String coachId){
        System.out.println("教练数据请求"+coachId);
        int id= Integer.parseInt(coachId);
        Coach coach =managerService.findCoach(id);
        model.addAttribute("coach",coach);
        return "CoachDetails";
    }
    @RequestMapping("/carDetails")
    public String carDetails(Model model, String carId){
        System.out.println("教练车数据请求"+carId);
        int id= Integer.parseInt(carId);
        Car car =managerService.findCar(id);
        model.addAttribute("car",car);
        return "CarDetails";
    }

}
